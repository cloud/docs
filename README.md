# docs
This is the repository for [https://docs.cloud.csclub.uwaterloo.ca](https://docs.cloud.csclub.uwaterloo.ca).

## Contributing
This is a [MkDocs](https://www.mkdocs.org) site, so all pages are writen in Markdown.

Install the prerequisites:
```sh
pip install mkdocs mkdocs-material pymdown-extensions
```

Start a local web server (http://localhost:8000):
```sh
mkdocs serve
```

Building the site:
```
mkdocs build
```
