# Virtual Hosting

[Virtual hosting](https://en.wikipedia.org/wiki/Virtual_hosting) is a method
for hosting multiple websites with different domain names on a single web
server. The idea is to redirect external traffic from the CSC web server to
your VM based on the domain name. If you are hosting a website on your VM
and you would like it to be publically accessible, you will need to create
a vhost record for your VM.

## Domain limitations
Each member may create up to 10 vhosts whose domains have the form
`username.csclub.cloud` or end with `-username.csclub.cloud`.

For example, if your username is `ctdalek`, then the following vhost domains
are all valid:

* ctdalek.csclub.cloud
* app-ctdalek.csclub.cloud
* abc-def-ctdalek.csclub.cloud

If you wish to have a more customized domain name, please see
[Using a custom domain name](#using-a-custom-domain-name), below.

!!! note "Multi-level subdomains"
    While you are allowed to create multi-level subdomains of csclub.cloud
    (e.g. app.ctdalek.csclub.cloud), we strongly ask that you do not do this
    unless you have a very good reason, because it is easier for us to use our
    existing wildcard SSL certificate which is valid for *.csclub.cloud (one
    level only).

## Creating a vhost record
Let's say your VM's IP address is 172.19.134.121 and you wish to use the domain
`ctdalek.csclub.cloud`. You must already have an application running on port 80
in your VM (e.g. NGINX).

Login to a [CSC general-use machine](https://wiki.csclub.uwaterloo.ca/Machine_List)
and run the following:
```sh
ceo cloud vhosts add ctdalek.csclub.cloud 172.19.134.121
```

!!! tip
    You can also append a port number to the IP address, e.g. `172.19.134.121:8000`.

If you specified a multi-level subdomain, this command will also provision a TLS
certificate for your website (from either [ZeroSSL](https://zerossl.com/) or
[Let's Encrypt](https://letsencrypt.org/)); otherwise, our existing wildcard
certificate will be used. You should now be able to visit your
website over HTTPS from your browser, e.g. `https://ctdalek.csclub.cloud`.

!!! warning
    To avoid having to reload our web server too frequently, and to reduce the risk
    of getting banned by our ACME service provider, we have set a rate limit on the
    command above for **once every three minutes**. We apologize for the inconvenience.

To view your vhost records, run the following:
```sh
ceo cloud vhosts list
```

To delete a vhost record, run the following:
```sh
ceo cloud vhosts delete ctdalek.csclub.cloud
```

!!! info
    If you run `ceo cloud vhost add` twice for the same domain, the second command
    will effectively override the first (that is, the vhost will be replaced).

## Using a custom domain name
If you wish to use a csclub.cloud domain which does not have your username in it,
please send an email to the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca)
with your desired domain name and the IP address of your VM.

If you wish to use a custom domain name which you have purchased from an external
registrar, please make it point to the IP addresses of `csclub.cloud`. As of this writing,
this is:

* IPv4: `129.97.134.10`
* IPv6: `2620:101:f000:4901:c5c::10ad`

Then, please send an email to the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca)
requesting a vhost record for your domain. Make sure to also provide the IP address
of your VM.
