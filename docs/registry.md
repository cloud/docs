# Registry
We are running an instance of [Harbor](https://goharbor.io/) at
[https://registry.cloud.csclub.uwaterloo.ca](https://registry.cloud.csclub.uwaterloo.ca).
Harbor is a container registry for uploading and downloading Docker images.

## Resource Limits
As of this writing, each member is restricted to a project with a 25GB
storage quota. Multiple images may be pushed to a project.

If you want more space, please send an email to syscom with a brief
justification.

## Logging in to the website
Just visit 
[https://registry.cloud.csclub.uwaterloo.ca](https://registry.cloud.csclub.uwaterloo.ca)
and press the 'Login via OIDC Provider' button.

## Create a project
*After* you have logged in to Harbor, SSH into a CSC machine and run the
following:
```sh
ceo registry project create
```
Now refresh the Harbor page; you should see a project whose name is your
username. You are an admin of this project, so you can invite other
Harbor users to it if you wish.

## Logging in to the registry
From the Harbor UI, click the top left corner, click 'User Profile', and
copy the CLI secret.

From your personal computer, login to the registry:
```sh
docker login registry.cloud.csclub.uwaterloo.ca
```
Use your CSC username for the username; use the CLI secret you copied earlier
for the password.

## Pushing an image
Once you have logged into the registry, you can start pushing your own images.

Let's say you have an image which you created locally called
`myimage:0.1.0`. Create a new tag of your image for the CSC registry:
```sh
docker tag myimage:0.1.0 registry.cloud.csclub.uwaterloo.ca/your_username/myimage:0.1.0
```
(Replace `your_username` with your actual username.)

Now push it:
```sh
docker push registry.cloud.csclub.uwaterloo.ca/your_username/myimage:0.1.0
```

!!! tip
    Keep your Docker images small. This speeds up your upload time, and makes
    you less likely to reach your storage quota.
    [Here](https://blog.codacy.com/five-ways-to-slim-your-docker-images/) are
    some good suggestions; you can find plenty more online with a quick
    Google search.
