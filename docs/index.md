# Welcome to the CSC Cloud!

The CSC cloud is a collection of compute, network and storage services
available to CSC members.

This service was made possible thanks to hardware donations and funding from:

* the Mathematics Endowment Fund (MEF)
* the Student Life Endowment Fund (SLEF)
* the Computer Science Computing Facility (CSCF)

## Available services

* [CloudStack](cloudstack) - Virtual machines on demand.
* [Virtual hosting](vhosts) - Domain name routing for websites.
* [Kubernetes](kubernetes) - Easily deploy your Dockerized apps.
* [Registry](registry) - A place to store your Docker images.

## Getting Started

First, make sure that [you are a CSC member](https://csclub.uwaterloo.ca/get-involved/)
and that your membership has not expired.

Here is a list of general-use CSC machines which you may SSH into after becoming
a member:
[https://wiki.csclub.uwaterloo.ca/Machine_List](https://wiki.csclub.uwaterloo.ca/Machine_List)

If you wish to see the terms for which your membership is valid, you can run the
following command from any general-use machine:
```sh
ceo members get <your_username>
```

!!! warning

    When your membership expires, all of your cloud resources will be **permanently deleted**,
    so make sure that you buy enough membership terms in advance.

To start creating your own virtual machines, please see the [CloudStack](cloudstack)
page for more details.
