# CloudStack

[Apache CloudStack](https://cloudstack.apache.org/) is an open-source
cloud computing platform which we use to allow members to create their
own virtual machines (VMs).

## Activating your cloud account
Before using CloudStack, you must activate your cloud account.
Log into any [general-use machine](https://wiki.csclub.uwaterloo.ca/Machine_List)
and run the following:
```sh
ceo cloud account activate
```
Now visit [https://cloud.csclub.uwaterloo.ca](https://cloud.csclub.uwaterloo.ca)
and login with your CSC username and password. For the domain, just enter 'Members'
(no quotes).

<img src="../img/cloudstack_login.png" alt="CloudStack login" width="400" />

## Account resource limits
As of this writing, the CloudStack resource limits for each member are:

* 8 CPU cores
* 8 GB of RAM
* 40 GB of disk space

If you wish to acquire more resources, please send an email to the Systems Committee
with a brief justification.

## Adding your SSH key
You will want to do this *before* creating a VM.

!!! note
    
    The rest of this section assumes that you already have an SSH key pair.
    If you do not have one yet, please create one first; there are plenty
    of good tutorials online on how to do this
    ([here](https://www.howtogeek.com/762863/how-to-generate-ssh-keys-in-windows-10-and-windows-11/)
    is one example).

Once you've logged into CloudStack, click on the 'Compute' button on the left-hand
panel (depending on how wide your screen is, you may only see a cloud icon),
then click 'SSH key pairs'. Click the 'Create a SSH Key Pair' button.

<img src="../img/cloudstack_ssh_key_pair_button.png" alt="CloudStack SSH key pair button" width="800" />

Now you will need to add your public key (this is the contents of the file which ends
with '.pub').

You can name the key pair anything you like; just make sure you remember the name.

<img src="../img/cloudstack_add_ssh_key_pair.png" alt="CloudStack add SSH key pair" width="400" />

Click OK once you have pasted your public key into the text box.

## Creating a VM
Click on the 'Compute' button on the left-hand panel, then click 'Instances'. Click
the 'Add Instance' button.

<img src="../img/cloudstack_add_instance_button.png" alt="CloudStack Add Instance button" width="800" />

Under the 'Template/ISO' section, choose the OS which you want your VM to run.

!!! tip

    Don't see your favourite OS listed? No problem! Just send an email to the
    [Systems Committee](mailto:syscom@csclub.uwaterloo.ca) requesting your OS
    to be added. We require that the OS must already have a publically available
    cloud image which has been prepared with [cloud-init](https://cloud-init.io/).

**Important**: Make sure to toggle the 'Override Root Disk Size' option and set it
to something reasonable (see [Account resource limits](#account-resource-limits) for
the maximum disk space which you can use.) The default root disk size for cloud
images is usually very small (e.g. 2GB), so you will definitely want to change it.

<img src="../img/cloudstack_root_disk_size.png" alt="CloudStack Root Disk Size" width="700" />

Now you need to choose a Compute Offering. If you are sure not sure how much
you need, we recommend that you start off with a Small or Medium instance; you
can always upgrade later if necessary.


<img src="../img/cloudstack_compute_offering.png" alt="CloudStack Compute Offering" width="700" />

You probably do not need to add an external disk. Be aware that if you do add one,
this counts towards your total disk quota.


<img src="../img/cloudstack_data_disk.png" alt="CloudStack Data Disk" width="700" />

Make sure that you do **NOT** place the VM in the default security group, because
this blocks all ingress traffic by default.

<img src="../img/cloudstack_security_groups.png" alt="CloudStack Security Groups" width="700" />

Choose the SSH keypair which you created earlier:

<img src="../img/cloudstack_choose_ssh_keypair.png" alt="CloudStack Choose SSH keypair" width="700" />

Enter a name for your VM:

<img src="../img/cloudstack_vm_details.png" alt="CloudStack VM details" width="700" />

Now press 'Launch Virtual Machine'.

If all goes well, you should see your VM running from the 'Instances' dashboard:
<img src="../img/cloudstack_running_instances.png" alt="CloudStack running instances" width="800" />

## Accessing your VM
All VMs will obtain an IP address from the 172.19.134.0/24 subnet (MC VLAN 425).
As this is a private IP range, you can only access this *directly* from on campus. If you
are not on campus, you can still access your VM via the following methods:

* use the [Campus VPN](https://uwaterloo.ca/information-systems-technology/services/virtual-private-network-vpn)
* use a general-use CSC machine as a jump host

The second option is generally more convenient. The idea is to SSH into a CSC
general-use machine first, then SSH from there into your VM.

From your personal machine, you can SSH into your VM by running a command like the following:
```sh
ssh -J ctdalek@corn-syrup.csclub.uwaterloo.ca debian@172.19.134.121
```
Replace `ctdalek` with your CSC username, replace `172.19.134.121` with the IP
address of your VM, and replace `debian` with the default username of the OS
which you chose:

* Debian: `debian`
* Ubuntu: `ubuntu`
* CentOS: `centos`

Once you have logged in, you can run `sudo -s` to become the root user.

See [SSH tricks](../ssh-tricks) for some useful SSH tricks.

## Next steps
Congratulations, you've created your VM! ...So what do you do now?

That's completely up to you! Since you can become the root user inside the
VM, you can install and run any software you want (just keep in mind that
you must still follow the
[Machine Usage Agreement](https://csclub.uwaterloo.ca/resources/machine-usage-agreement/)).

Here are some ideas to get you started:

* Install [Docker](https://docs.docker.com/engine/install/) and run some containers
* Install [Apache](https://httpd.apache.org/) or [NGINX](http://nginx.org/) and
  serve a static website (e.g. a blog)
* Run a Node.js/Python/Golang/whatever web application
* Install [k0s](https://k0sproject.io/) and run a small Kubernetes cluster

The world's your oyster! 😊

If you plan on hosting a publicly available website, you will want to create
a virtual host. See [Virtual Hosting](../vhosts) for details.

## Limitations
There are a few limitations of our CloudStack setup which are worth knowing:

* The console proxy will only work if you're on the campus network. See
  [SSH Tricks](../ssh-tricks) for instructions on how to set up a SOCKS proxy.
* If you're uploading a template or ISO from your computer, you also need
  to be on the campus network.
* If you're creating a template which downloads an OS image from a URL, and
  that URL resolves to a machine on campus, it won't work. There's some
  iptables setup which we (syscom) need to do beforehand; just send us an
  email if you're going to be downloading a template from an on-campus
  server.
