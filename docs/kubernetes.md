# Kubernetes

We are running a [Kubernetes](https://k8s.io) cluster on top of CloudStack.
Each member gets their own namespace in which they can deploy their applications.

!!! info
    If you wish to learn about how to operate a Kubernetes cluster, you may
    create your own cluster using software such as [k3s](https://k3s.io). If
    all you want to do is run some containerized apps, then we suggest that you
    use our cluster instead, since it has far more resources.

## Account resource limits
As of this writing, the per-namespace Kubernetes resource limits for each member are:

* 40 pods
* 10 jobs
* 10 cron jobs
* 25 GB of Persistent Volume Claims
* 5 NodePort services

If you wish to acquire more resources, please send an email to the Systems Committee
with a brief justification.

!!! info
    LoadBalancer services are disabled because we are running our own load balancer
    (NGINX) outside of Kubernetes which we manage ourselves.
    See [Exposing your app](#exposing-your-app), below.


## Create a new namespace

Log into a general-use machine and run the following:
```sh
ceo k8s account activate
```

This will create a new Kubernetes namespace whose name has the format `csc-username`.
A new kubeconfig file will be placed into `~/.kube/config`. To verify that everything
is working properly, run the following command:
```sh
kubectl cluster-info
```

The output should look something like this:
```
Kubernetes control plane is running at https://172.19.134.149:6443
CoreDNS is running at https://172.19.134.149:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

## Quickstart

So, let's say you've created a Dockerized app, and the image is available on some publicly available
registry such as [Docker Hub](https://hub.docker.com) or [Quay.io](https://quay.io). Here is the bare
minimum which you need to do get your app running in our Kubernetes cluster.

First, create a Deployment:
```sh
kubectl create deployment demo --image=ctdalek/myapp --port=80
```
Replace the values of `--image` and `--port` as necessary.

If your app does not need to be publicly exposed, then you are done; otherwise, you need to create
a Service to expose your Deployment:
```sh
kubectl expose deployment demo
```

Finally, create an Ingress which exposes the Service to the outside world:
```sh
kubectl create ingress demo --rule='ctdalek.k8s.csclub.cloud/*=demo:80'
```

You should now be able to access your app at e.g. `https://ctdalek.k8s.csclub.cloud`.

To see what kind of Ingress domains you may create, see [Ingress domains](#ingress-domains)
below.

## Not-so-quick start

At this point I am going to take the lazy way out and direct you to the official
Kubernetes documentation:

* [https://kubernetes.io/docs/home/](https://kubernetes.io/docs/home/)
* [https://kubernetes.io/docs/tutorials/](https://kubernetes.io/docs/tutorials/)
* [https://kubectl.docs.kubernetes.io/guides/](https://kubectl.docs.kubernetes.io/guides/)

You may also contact the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca) for
assistance if something is not working the way you think it should.

## Exposing your app

If your app is running in our Kuberenetes cluster, you have two options for exposing
it to the outside world: an [Ingress](https://kubernetes.io/docs/concepts/services-networking/ingress/)
(preferred), or a [NodePort](https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport).
We generally recommend using an Ingress if your program is a web application; for
non-web apps, you will need to use a NodePort instead. Since our Kubernetes cluster
is not publicly accessible, the NodePort will only be directly accessible from on-campus.

### Ingress domains

By default, you may only create Ingress rules of the form `username.k8s.csclub.cloud`
or `*-username.k8s.csclub.cloud`. If you wish to use a custom domain, please
contact the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca).

If you decide to create a multi-level subdomain (e.g. `app.ctdalek.k8s.csclub.cloud`),
then you will also need to create a corresponding virtual host (see
[Virtual Hosting](../vhosts)) which points to `k8s`. For example:
```sh
ceo cloud vhosts add app.ctdalek.k8s.csclub.cloud k8s
```

We strongly encourage you to use single-level subdomains instead
(e.g. `app-ctdalek.k8s.csclub.cloud`) since this does not require us to obtain a
new SSL certificate.
