# SSH Tricks

Here are some useful SSH tricks for accessing your VM.

The commands below assume that the SSH public key on your personal machine
has already been copied to two places:

1. the `~/.ssh/authorized_keys` file in your CSC home directory
2. the `~/.ssh/authorized_keys` file of the default user in your VM

!!! Note
    If you are having trouble with any of the commands below, please don't
    hesitate to ask the [Systems Committee](mailto:syscom@csclub.uwaterloo.ca)
    for assistance.

Most of the "tricks" below require ProxyJump to have been setup, so we suggest
reading that first.

## ProxyJump
To avoid having to manually SSH to a CSC machine before SSH'ing to your
VM, you can use the ProxyJump directive. For example, let's say your VM's
IP address is `172.19.134.121`, and you want to use `corn-syrup` as a jump
host.

Add a snippet similar to the following in your `~/.ssh/config` (on your
personal machine):
```sh
Host corn-syrup
    HostName corn-syrup.csclub.uwaterloo.ca
    # Replace this with your username
    User ctdalek
Host ctdalek-vm1
    # Replace this with the IP address of your VM
    HostName 172.19.134.121
    ProxyJump corn-syrup
    # Replace this with the default user in your VM
    User debian
```

Now you can connect to your VM by running
```sh
ssh ctdalek-vm1
```

!!! Note
    If the name of your SSH key is not one of the default names (e.g. id_rsa,
    id_ed25519), you may also need to specify the `IdentityFile` option.

## Port forwarding
Let's say you have a process bound to `localhost:8000` in your VM, and you'd like
to access it from your personal machine. Then you just need to run the following:
```sh
ssh -L 8000:localhost:8000 ctdalek-vm1
```
This will forward requests to `localhost:8000` on your personal machine to
`localhost:8000` on your VM.

If you want to fork the process to the background, here's one way to do it:
```sh
ssh -L 8000:localhost:8000 -CNfq ctdalek-vm1
```
Explanation:

* `-C`: compress (saves bandwidth)
* `-N`: don't execute a command on the server
* `-f`: fork the SSH process to the background
* `-q`: quiet (silences output)

## Reverse port forwarding
Let's say you have a process bound to `localhost:8000` on your personal machine,
and you'd like to access it from your VM. Instead of using `-L`, you want to use
`-R` instead:
```sh
ssh -R 8000:localhost:8000 -CNfq ctdalek-vm1
```
This will forward requests to `localhost:8000` in the VM to `localhost:8000` on
your personal machine.

## SOCKS proxy
You probably won't need this one, but it's good to know. This basically allows
you to use a CSC machine as a proxy for *all* of your Internet traffic for a
particular application. It's useful when you need to access a website which
is only available from the campus network.

First, let's run a SOCKS proxy on e.g. `localhost:8132`:
```sh
ssh -D 8132 -CNfq corn-syrup
```
You now need to configure your application to use the proxy. For example, in
Firefox, you can do the following:

* Visit `about:preferences` in the URL bar
* Scroll to the bottom, and click the Settings button under 'Network Settings'
* Select 'Manual proxy configuration'
* Enter 'localhost' as the SOCKS Host, and 8132 for the port. Also make sure
  'SOCKS v5' is selected.

After pressing 'OK', you should now be able to visit websites using a campus
IP address.
